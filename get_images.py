import datetime

import requests




def download_image(url):  # 同步
    print("开始下载：", url)
    response = requests.get(url)
    print("下载完成")
    # 图片保存到本地
    file_name = url.rsplit('8888')[1].rsplit("/")[2]
    with open(f"/mnt/c/Users/LIULI/Pictures/Yolo/data/images/{file_name}", mode='wb') as file_object:
        file_object.write(response.content)


if __name__ == '__main__':
    url_list = [
        'http://10.0.151.27:8888/10.0.151.241/10.0.151.241_01_20210928145834338_TIMING.jpg',
        'http://10.0.151.27:8888/10.0.151.241/10.0.151.241_01_20210928151334320_TIMING.jpg',
        'http://10.0.151.27:8888/10.0.151.241/10.0.151.241_01_20210928152834342_TIMING.jpg',
        'http://10.0.151.27:8888/10.0.151.241/10.0.151.241_01_20210928145834338_TIMING.jpg',
        'http://10.0.151.27:8888/10.0.151.241/10.0.151.241_01_20210928151334320_TIMING.jpg',
        'http://10.0.151.27:8888/10.0.151.241/10.0.151.241_01_20210928152834342_TIMING.jpg'
    ]
    start_time = datetime.datetime.now()
    for i in url_list:
        download_image(i)
    end_time = datetime.datetime.now()

    print("总共耗时：", end_time - start_time)
