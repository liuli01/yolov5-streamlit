#!/bin/bash
rule="person"
path="/home/liuli/apps/yolov5-streamlit/"
IFS=$'\n\n'
for line in `cat $1`
do
   if [[ $line == *$rule* ]]
   then
        echo $line >>$2
        IFS=$' '
		array=(${line//:/ }) 
		cp $path${array[0]} $3 
		echo ${array[0]}
   fi
done
echo '执行结束  请查看'$3
