import datetime

import aiohttp
import asyncio

import requests
from lxml import html
etree = html.etree


# 获取站点ip
url_ip='http://10.0.151.27:8888/'
data_ip=requests.get(url_ip).text
etree_ip=etree.HTML(data_ip)


# ip 地址
ip_list=[]
for i in range(5,3300):
    ip=etree_ip.xpath(f'/html/body/pre/a[{i}]/@href')
    # 筛选所有的
    # if str(ip).find("172")==-1:
    # 筛选溧阳的，倒数第二个ip地址在0-32个之间
    if str(ip).find("172")==-1 or int(str(ip).split("172.16.")[1].split(".")[0])>32:
        pass
    else:
        ip_list.append(ip[0])



# 日期
date_list=[]
for ip in ip_list:
    date_url = f'http://10.0.151.27:8888{ip}'
    date_date = requests.get(date_url).text
    date_etree=etree.HTML(date_date)
# 通过@href获取他的href标签里面的内容
    for j in range(100,103):
        date=date_etree.xpath(f'/html/body/pre/a[{j}]/@href')
        if str(date).find("172")==-1 or str(date).find("test")!=-1:
            pass
        else:
            date_list.append(date[0])



# 图像image
image_list=[]
for date in date_list:
    image_url= f'http://10.0.151.27:8888{date}'
    image_date = requests.get(image_url).text
    image_etree=etree.HTML(image_date)
    #通过@href获取他的href标签里面的内容
    for j in range(0,96):
        image=image_etree.xpath(f'/html/body/pre/a[{j}]/@href')
        if str(image).find("jpg")==-1:
            pass
        else:
            image_list.append(image[0])



url_list=[]
for i in image_list:
    url_list.append(f'http://10.0.151.27:8888{i}')

# 异步
async def fetch(session, url):
    print("发送请求：", url)
    async with session.get(url, verify_ssl=False) as response:
        content = await response.content.read()
        file_name = url.rsplit('8888')[1].rsplit("/")[3]
        # with open(f"/mnt/c/Users/LIULI/Pictures/Yolo/data/images/{file_name}", mode='wb') as file_object:
        with open(f"data/images/{file_name}", mode='wb') as file_object:
            file_object.write(content)
        print("下载完成", url)

async def main():
    async with aiohttp.ClientSession() as session:

        tasks = [asyncio.create_task(fetch(session, url)) for url in url_list]

        await asyncio.wait(tasks)


if __name__ == '__main__':
    start_time = datetime.datetime.now()

    asyncio.run(main())

    end_time = datetime.datetime.now()

    print("总共耗时：",end_time - start_time)
